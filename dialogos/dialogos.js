'use strict'
const fs = require('fs');
var dialogos = function(tipo, title, mensaje, resolve) {
    if ($('#dialogos').length === 0) {
        var contenthtml = fs.readFileSync(`${__dirname}/dialogos.html`, 'utf-8');
        $(contenthtml).appendTo(document.body);
    }
    var txt;
    if (tipo === 'Prompt') {
        $('#Input').removeClass('is-hidden');
        $('#buttonpromptCANCEL').removeClass('is-hidden');
    }
    $('#titlePrompt').html(title);
    $('#Info').html(mensaje);
    $("#dialogos").addClass('is-active');
    $('#Input').attr('type = password');
    $('#Input').focus();
    $("#buttonpromptOK").click(function() {
        txt = $("#Input").val();
        restoreAll();
        resolve(txt);
    });
    $("#buttonpromptCANCEL").click(function() {
        restoreAll();
        resolve();
    });
    $(document).keyup(function(event) {
        event.preventDefault();
        if (event.keyCode === 13) { //Enter key
            $("#buttonpromptOK").click();
        } else if (event.keyCode === 27) { //Esc key
            $("#buttonpromptCANCEL").click();
        }
    });
}

function restoreAll() {
    $('#Input').val('');
    $('#Info').text('');
    $('#Input').addClass('is-hidden');
    $('#buttonpromptCANCEL').addClass('is-hidden');
    $("#dialogos").removeClass('is-active');
    $('#buttonpromptCANCEL').unbind('click');
    $('#buttonpromptOK').unbind('click');
    $(document).unbind('keyup');
}

module.exports = dialogos;