let decode = require('image-decode')
let encode = require('image-encode')
const testFolder = './ORIGINAL/';
const testFolder2 = './MODIFIED/';
var $ = require('jquery')
const fs = require("fs")
var image=""
var key0
var key1
var key2
var key3

function ModImage(key0,key1,key2,key3){
    let {data, width, height} = decode(fs.readFileSync(image))
    for(var i=0;i<data.length;i+=4){
        data[i]=corrimiento(key0,data[i],256)
        data[i+1]=corrimiento(key1,data[i+1],256)
        data[i+2]=corrimiento(key2,data[i+2],256)
        data[i+3]=corrimiento(key3,data[i+3],256)
    }
    fs.writeFileSync(
        './MODIFIED/modded.bmp',
        Buffer.from(encode(data, [width,height],'bmp'))
    )
    fs.readdirSync(testFolder2).forEach(file => {
        d = new Date();
        $("#img2").attr("src",testFolder2+file+"?"+d.getTime());
    });
}

function corrimiento(key,code){
    return  (key+code)%256
}

$(() => {
        $("#ENC").click(function(){
            console.log(key0,key1,key2,key3)
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    $("#img1").attr("src",testFolder+file);
                    image=testFolder+file
                })
                ModImage(key0,key1,key2,key3)
            }else{
                alert("Not valid key")
            }
        })
        $("#DEC").click(function(){
            console.log(key0,key1,key2,key3)
            if(checkKeys()){
                fs.readdirSync(testFolder).forEach(file => {
                    $("#img1").attr("src",testFolder+file);
                    image=testFolder+file
                })
                ModImage(256-key0,256-key1,256-key2,256-key3)
            }else{
                alert("Not valid key")
            }
        })
})

function checkKeys(){
    key0 = parseInt($("#key0").val())
    key1 = parseInt($("#key1").val())
    key2 = parseInt($("#key2").val())
    key3 = parseInt($("#key3").val())
    arr = [key0,key1,key2,key3]
    for(var elem of arr){
        if(isNaN(elem)){
            return false
        }
    }
    return true
}